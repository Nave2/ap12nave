#include "threads.h"

using std::cout;
using std::endl;
using std::mutex;
using std::vector;

mutex outFileMtx;

bool isPrime(int num)
{
	int end = sqrt(num); //efficenecy!!!
	for (int i = 2; i <= end; i++)
		if (num % i == 0)
			return false;
	return true;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	for (int i = begin; i <= end; i++)
		if (isPrime(i))
		{
			outFileMtx.lock();
			file << i << endl;
			outFileMtx.unlock();
		}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file;
	file.open(filePath);
	int jump = (end - begin) / double(N);
	int i = 0;
	clock_t start = clock();
	vector<std::thread*> threads;
	for (i = begin; i <= end; i += jump + 1)
	{
		//cout << "thread " << i << endl;
		int endThread = (i + jump > end) ? end : i + jump;
		std::thread thread(writePrimesToFile, i, endThread, std::ref(file));
		thread.join();
	}
	clock_t finish = clock();
	cout << "callWritePrimesMultipleThreads() ran  " << (finish - start) / (double)CLOCKS_PER_SEC << "s." << endl;
	file.close();
}
