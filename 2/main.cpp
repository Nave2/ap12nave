#include "MessagesSender.h"

int main()
{
	MessagesSender ms;
	thread updateMsgsT(&MessagesSender::updateMsgs, std::ref(ms));
	thread spreadMsgsT(&MessagesSender::spreadMsgs, std::ref(ms));

	while (ms.printMenu() != 4) {};
	cout << "bye bye" << endl;

	updateMsgsT.detach();
	spreadMsgsT.detach();
	return 0;
}