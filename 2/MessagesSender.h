#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <fstream>
#include <iterator>
#include <queue>
#include <chrono>
#include <string>
#include <mutex>
#include <condition_variable>
#pragma once

using namespace std;

class MessagesSender
{
public:
	mutex _msgsMtx;
	condition_variable _msgsCV;
	mutex _usersMtx;
	ifstream _data;
	vector<string> _users;
	queue<string> _msgs;

	void signing();
	void signout();
	void printUsers() const;


public:
	void spreadMsgs();
	void updateMsgs();
	int printMenu();
};