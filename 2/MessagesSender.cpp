#include "MessagesSender.h"


string getUsername()
{
	string username;
	cout << "Enter username: ";
	cin >> username;
	return username;
}

void MessagesSender::signing()
{
	this->_usersMtx.lock();
	string username = getUsername();
	if (find(this->_users.begin(), this->_users.end(), username) == this->_users.end())
		this->_users.push_back(username);
	else
		cerr << "such user already exists." << endl;
	this->_usersMtx.unlock();
}

void MessagesSender::signout()
{
	this->_usersMtx.lock();
	string username = getUsername();
	this->_users.erase(std::remove(this->_users.begin(), this->_users.end(), username), this->_users.end());
	this->_usersMtx.unlock();
}

void MessagesSender::printUsers() const
{
	copy(this->_users.begin(), this->_users.end(), ostream_iterator<string>(cout, ", "));
	cout << endl;
}

int MessagesSender::printMenu()
{
	cout << "Choose an option:" << endl <<
		"	1. Signing" << endl <<
		"	2. Signout" << endl <<
		"	3. Connected Users" << endl <<
		"	4. exit" << endl;
	int option;
	cin >> option;

	switch (option)
	{
	case 1:
		signing();
		break;
	case 2:
		signout();
		break;
	case 3:
		printUsers();
		break;
	default:
		break;
	}

	return option;
}

void MessagesSender::updateMsgs()
{
	while (true) {
		this->_data.open("data.txt", std::ios::in);
		string line;

		unique_lock<mutex> lock(this->_msgsMtx);
		while (getline(this->_data, line)) {
			this->_msgs.push(line);
		}
		lock.unlock();
		this->_msgsCV.notify_all();
		this->_data.close();
		this->_data.open("data.txt", std::ofstream::out | std::ofstream::trunc);
		this->_data.close();
		this_thread::sleep_for(chrono::seconds(10));
	}
}

void MessagesSender::spreadMsgs()
{
	ofstream out_f;
	out_f.open("output.txt", std::ofstream::out | std::ofstream::trunc);
	out_f.close();
	while (true) {
		this->_usersMtx.lock();
		unique_lock<mutex> lock(this->_msgsMtx);
		this->_msgsCV.wait(lock, []() {return true; });
		while (!this->_msgs.empty())
		{
			string msg = this->_msgs.front();
			this->_msgs.pop();

			ofstream out;
			out.open("output.txt", std::ios::app);
			for (string username : this->_users) {
				out << username << ": " << msg << endl;
			}
			out.close();
		}
		lock.unlock();
		this->_usersMtx.unlock();
		this_thread::sleep_for(chrono::seconds(2));
	}
}